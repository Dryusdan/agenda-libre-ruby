# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180408212139) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_id", null: false
    t.string "resource_type", null: false
    t.integer "author_id"
    t.string "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "cities", force: :cascade do |t|
    t.string "name", limit: 255, default: "", null: false
    t.string "majname", limit: 255, default: "", null: false
    t.integer "postalcode", limit: 4
    t.integer "inseecode", limit: 4
    t.integer "regioncode", limit: 4
    t.float "latitude", limit: 24
    t.float "longitude", limit: 24
    t.index ["name"], name: "cities_name"
  end

  create_table "events", force: :cascade do |t|
    t.string "title", limit: 255, default: "", null: false
    t.text "description", limit: 65535, null: false
    t.datetime "start_time", null: false
    t.datetime "end_time", null: false
    t.string "city", limit: 255, default: ""
    t.integer "region_id", limit: 4, default: 0, null: false
    t.integer "locality", limit: 4, default: 0, null: false
    t.string "url", limit: 255, default: ""
    t.string "contact", limit: 255, default: ""
    t.string "submitter", limit: 255, default: "", null: false
    t.integer "moderated", limit: 4, default: 0, null: false
    t.text "tags", limit: 255, default: ""
    t.string "secret", limit: 255, default: "", null: false
    t.datetime "decision_time"
    t.datetime "submission_time"
    t.string "moderator_mail_id", limit: 32
    t.string "submitter_mail_id", limit: 32
    t.text "address", limit: 65535
    t.float "latitude", limit: 24
    t.float "longitude", limit: 24
    t.integer "lock_version", limit: 4, default: 0, null: false
    t.string "place_name", limit: 255
    t.integer "count", default: 1
    t.integer "repeat", default: 0
    t.text "rule"
    t.integer "event_id"
    t.index ["event_id"], name: "index_events_on_event_id"
    t.index ["start_time", "end_time"], name: "events_date"
  end

  create_table "kinds", force: :cascade do |t|
    t.string "name", null: false
    t.string "icon"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notes", force: :cascade do |t|
    t.text "contents", limit: 65535, null: false
    t.datetime "date", null: false
    t.integer "event_id", limit: 4
    t.integer "author_id", limit: 4
  end

  create_table "orgas", force: :cascade do |t|
    t.integer "region_id", limit: 4, default: 0, null: false
    t.string "department", limit: 4, default: "0", null: false
    t.string "name", limit: 255, default: "", null: false
    t.string "url", limit: 255, default: "", null: false
    t.string "city", limit: 255, default: ""
    t.integer "kind_id", limit: 4
    t.string "feed", limit: 255
    t.string "contact", limit: 255
    t.string "submitter", limit: 255
    t.boolean "moderated", limit: 1, default: false
    t.datetime "submission_time"
    t.datetime "decision_time"
    t.string "secret", limit: 255
    t.boolean "deleted", limit: 1, default: false, null: false
    t.boolean "active", default: true, null: false
    t.text "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text "tag"
    t.text "tags", default: ""
    t.text "diaspora"
    t.text "object_changes"
    t.text "place_name"
    t.text "address"
    t.float "latitude"
    t.float "longitude"
    t.index ["kind_id"], name: "index_orgas_on_kind_id"
  end

  create_table "regions", force: :cascade do |t|
    t.string "name", limit: 255, default: "", null: false
    t.integer "region_id"
    t.string "code"
    t.string "url"
    t.index ["region_id"], name: "index_regions_on_region_id"
  end

  create_table "taggings", force: :cascade do |t|
    t.integer "tag_id"
    t.integer "taggable_id"
    t.string "taggable_type"
    t.integer "tagger_id"
    t.string "tagger_type"
    t.string "context", limit: 128
    t.datetime "created_at"
    t.index ["context"], name: "index_taggings_on_context"
    t.index ["tag_id"], name: "index_taggings_on_tag_id"
    t.index ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context"
    t.index ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy"
    t.index ["taggable_id"], name: "index_taggings_on_taggable_id"
    t.index ["taggable_type"], name: "index_taggings_on_taggable_type"
    t.index ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type"
    t.index ["tagger_id"], name: "index_taggings_on_tagger_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string "name"
    t.integer "taggings_count", default: 0
  end

  create_table "translations", force: :cascade do |t|
    t.string "locale", limit: 255
    t.string "key", limit: 255
    t.text "value", limit: 65535
    t.text "interpolations", limit: 65535
    t.boolean "is_proc", limit: 1, default: false
  end

  create_table "users", force: :cascade do |t|
    t.string "login", limit: 255, default: "", null: false
    t.string "password", limit: 255, default: "", null: false
    t.string "email", limit: 255, default: "", null: false
    t.string "lastname", limit: 255, default: "", null: false
    t.string "firstname", limit: 255, default: "", null: false
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.datetime "remember_created_at"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.text "object_changes", limit: 1073741823
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

end

# Formatter for diff output
module Differ
  module Format
    # Try to format as unified diff, using - and +
    module Patch
      class << self
        def format(change)
          if change.change?
            as_change change
          elsif change.delete?
            as_delete change
          elsif change.insert?
            as_insert change
          else
            ''
          end
        end

        private

        def as_insert(change)
          if change.insert.start_with? ' '
            change.insert[0] = '+'
            change.insert
          else
            '+ ' + change.insert
          end
        end

        def as_delete(change)
          if change.delete.start_with? ' '
            change.delete[0] = '-'
            change.delete
          else
            '- ' + change.delete
          end
        end

        def as_change(change)
          [as_delete(change), as_insert(change)].join "\n"
        end
      end
    end
  end
end

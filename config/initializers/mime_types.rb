# Be sure to restart your server when you modify this file.

# Add new mime types for use in respond_to blocks:
# Mime::Type.register "text/richtext", :rtf
# Necessary for the /ical.php redirect to /events.ics
Mime::Type.register 'text/calendar', :ics, [], %w[ical]

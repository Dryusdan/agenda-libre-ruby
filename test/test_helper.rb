ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../config/environment', __dir__)
require 'rails/test_help'

# require 'simplecov'

# SimpleCov.start 'rails'

module ActiveSupport
  # Standard helper class for almost all tests
  class TestCase
    ActiveRecord::Migration.check_pending!

    # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical
    # order.
    #
    # Note: You'll currently still have to declare fixtures explicitly in
    # integration tests
    # -- they do not yet inherit this setting
    fixtures :all

    # Add more helper methods to be used by all tests here...

    Geocoder.configure lookup: :test

    Geocoder::Lookup::Test.set_default_stub [{
      'latitude'     => 40.7143528,
      'longitude'    => -74.0059731,
      'address'      => 'New York, NY, USA',
      'state'        => 'New York',
      'state_code'   => 'NY',
      'country'      => 'United States',
      'country_code' => 'US'
    }]
  end
end

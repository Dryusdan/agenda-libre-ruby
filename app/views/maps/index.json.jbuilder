json.array!(@events) do |event|
  json.merge!(
    type: 'Feature',
    properties: {
      id: event.id,
      name: event.title,
      start_time: event.start_time, end_time: event.end_time,
      submission_time: event.submission_time,
      decision_time: event.decision_time,
      place_name: event.place_name, address: event.address, city: event.city,
      region: event.region.name, region_id: event.region_id,
      tags: event.tag_list,
      popupContent: "<a href=\"/events/#{event.id}\">#{event}</a>"
    },
    geometry: {
      type: 'Point',
      coordinates: [event.longitude, event.latitude]
    }
  )
end

# Manage event and organisation tags
class TagsController < InheritedResources::Base
  has_scope :region, :locality, :daylimit
  has_scope :period, type: :hash, using: %i[year week]
  has_scope :tag, as: :id

  def index
    @tags = ActsAsTaggableOn::Tag.where('taggings_count > 3')
    if params[:term]
      # Used to autocomplete tags
      @tags = @tags.select(:id, :name, 'name AS label')
                   .where('name LIKE ?', "#{params[:term]}%")
    end

    respond_to do |format|
      format.html
      format.json { render json: @tags }
    end
  end

  def show
    @events_future = apply_scopes Event.moderated.future
    @events_past = apply_scopes Event.moderated.past

    respond_to do |format|
      format.html
      format.json { render json: @events_future + @events_past }
    end
  end

  def orgas
    @tags = organise_tags apply_scopes(Orga.moderated)

    respond_to do |format|
      format.html
      format.json { render json: @tags }
    end
  end

  private

  # Splits, groups, rejects the less used
  def organise_tags(tags)
    tags.where.not(tags: '').pluck(:tags).map(&:split).flatten
        .group_by { |i| i }
        .map { |k, v| [k, v.size] }
        .reject { |_k, v| v <= 3 }
        .sort { |t| -t[1] }
  end
end

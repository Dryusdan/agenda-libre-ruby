# Events, particulary during moderation, can have notes associated to them
class NotesController < ApplicationController
  before_action :set_event, only: %i[new create]
  before_action :create_note, :set_mailer_host, only: %i[create]

  # GET /moderations/id/new
  def new
    @note = @moderation.notes.new
  end

  def create
    respond_to do |format|
      if @note.save && send_mails
        format.html { redirect_to moderations_url, notice: t('.ok') }
        format.json { render action: :show, status: :created, location: @event }
      else
        format.html { render action: :new }
        format.json { render json: @note.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_event
    @event = Event.find params[:moderation_id]
    @moderation = @event
  end

  def create_note
    @note = @moderation.notes.new note_params.merge author: current_user
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def note_params
    params.require(:note).permit :contents
  end

  # Useful to manage absolute url in mails
  def set_mailer_host
    ActionMailer::Base.default_url_options[:host] = request.host_with_port
  end

  def send_mails
    if params[:envoiParMail] == 'oui'
      # Send an update mail to its author
      NoteMailer.notify(@note).deliver_now
      @note.contents = t '.sendByMailWrap', contents: @note.contents
      @note.save
    end
    NoteMailer.create(@note).deliver_now
  end
end

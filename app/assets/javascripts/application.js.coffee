# This is a manifest file that'll be compiled into application.js, which will include all the files
# listed below.
#
# Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
# or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
#
# It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
# compiled file.
#
# Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
# about supported directives.
#
#= require jquery
#= require jquery_ujs
#= require jquery-sparkline
# For tags input
#= require jquery-ui/widgets/autocomplete
#= require jquery.tagsinput
#= require turbolinks
#= require tinymce-jquery
#= require modernizr
#= require leaflet
#= require leaflet.markercluster
#= require leaflet.awesome-markers
#= require frTypo
#= require_tree .

$(document).on 'turbolinks:load', ->
  # Hides the chrome broken image when image is absent
  if !Modernizr.testAllProps('forceBrokenImageIcon')
    $('img.favicon').one 'error', ->
      $(this).css visibility: 'hidden'

  $('.field.tags input').tagsInput
    delimiter: ' '
    defaultText: ''
    autocomplete_url: '/tags.json'
    onChange: ->
      value = $(this).val()
      if value.indexOf(',') >= 0
        $(this).val(value.replace /,/, '')

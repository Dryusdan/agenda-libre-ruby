# Sending mails related to events' moderation
class ModerationMailer < ApplicationMailer
  helper :events

  def create(event)
    @event = event

    mail 'Message-ID' =>
      "<mod-#{event.id}@#{ActionMailer::Base.default_url_options[:host]}>",
         subject: subject(event)
  end

  def update(event)
    @event = event
    @current_user = User.find_by id: event.paper_trail.originator

    mail 'In-Reply-To' =>
      "<mod-#{event.id}@#{ActionMailer::Base.default_url_options[:host]}>",
         subject: subject(event)
  end

  def accept(event)
    @event = event
    @current_user = User.find_by id: event.paper_trail.originator

    mail 'In-Reply-To' =>
      "<mod-#{event.id}@#{ActionMailer::Base.default_url_options[:host]}>",
         subject: subject(event)
  end

  def destroy(event)
    @event = event
    @current_user = User.find_by id: event.paper_trail.originator

    mail 'In-Reply-To' =>
      "<mod-#{event.id}@#{ActionMailer::Base.default_url_options[:host]}>",
         subject: subject(event)
  end

  private

  def subject(event)
    t('mail_prefix') +
      t("#{mailer_name}.#{action_name}.subject", subject: event.title)
  end
end
